# nuxt2 + element dashboard

![输入图片说明](https://cdn.nlark.com/yuque/0/2019/png/160590/1546762262087-845c1e37-9aa7-4f1d-b1ec-6627411c8c24.png#align=left&display=inline&height=227&linkTarget=_blank&name=image.png&originHeight=454&originWidth=1980&size=132203&width=990 "在这里输入图片标题")

## Docs

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

## Build Setup

```bash
# install dependencies
$ yarn

# serve with hot reload at localhost:3000
# using mock api to develop
$ yarn mock

# using mock api to develop which doesn't need login
$ yarn mock:nologin

# using backend api to develop
$ yarn dev

# build for production
$ yarn build
```

## code style

when you npm i or yarn, prettier has already installed

the configuration file is .prettierrc

Pre-commit Hook use [pretty-quick](https://github.com/azz/pretty-quick), maybe commit in terminal will be better, in IDE
like webstorm it may get confused behavior😕
